﻿class {caret} A
{
  public const string A;

  public void A()
  {
  }
  
  public void G()
  {
  }
  
  #region TestC
  
  public void C()
  {
  }
  
  public const string D;
  
  public string C {get; set;}

  public const string C;
  
  #endregion

  public void H()
  {
  }

  #region TestA
  
  public void B()
  {
  }
  
  public string B {get; set;}
  
  public string A {get; set;}

  public const string B;
 
  #endregion
  
  public void L()
  {
  }

  public void M()
  {
  }

  public void O()
  {
  }

  public void Q()
  {
  }
  
  #region TestB
  
  public void F()
  {
  }
  
  public string D {get; set;}

  public string E {get; set;}
  
  public void D()
  {
  }

  public const string E;
   
  #endregion

  public void T()
  {
  }
}