class {caret} A
{
  [Test5]
  private string A;

  [Test6]
  public event PropertyChangedEventHandler PropertyChanged;

  [Test4]
  private string B {get; set;}

  [Test1]
  private const string Z;

  [Test2]
  private A()
  {
  
  }

  [Test3]
  private string D()
  {
  
  }
}