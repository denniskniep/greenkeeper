class A
{
  public A(string arg1{off})
  {             
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
  }  
  
  public A(string arg1,string arg2{off})
  {
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(arg2, null)) throw new ArgumentNullException("arg2");
  } 
  
  public A(string arg1,string arg2,string arg3{off})
  {
    if (ReferenceEquals(arg2, null)) throw new ArgumentNullException("arg2");
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(arg3, null)) throw new ArgumentNullException("arg3");
  } 

  public void A(string arg1{off})
  {
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
  }  
  
  public void A(string arg1,string arg2{off})
  {
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(arg2, null)) throw new ArgumentNullException("arg2");
  } 
  
  public void A(string arg1,string arg2,string arg3{off})
  {
    if (ReferenceEquals(arg2, null)) throw new ArgumentNullException("arg2");
	if (ReferenceEquals(arg1, null)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(arg3, null)) throw new ArgumentNullException("arg3");
  } 
}

class B
{
  public B(string arg1{off})
  {             
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
  }  
  
  public B(string arg1,string arg2{off})
  {
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(null, arg2)) throw new ArgumentNullException("arg2");
  } 
  
  public B(string arg1,string arg2,string arg3{off})
  {
    if (ReferenceEquals(null, arg2)) throw new ArgumentNullException("arg2");
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(null, arg3)) throw new ArgumentNullException("arg3");
  } 

  public void B(string arg1{off})
  {
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
  }  
  
  public void B(string arg1,string arg2{off})
  {
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(null, arg2)) throw new ArgumentNullException("arg2");
  } 
  
  public void A(string arg1,string arg2,string arg3{off})
  {
    if (ReferenceEquals(null, arg2)) throw new ArgumentNullException("arg2");
	if (ReferenceEquals(null, arg1)) throw new ArgumentNullException("arg1");
	if (ReferenceEquals(null, arg3)) throw new ArgumentNullException("arg3");
  } 
}