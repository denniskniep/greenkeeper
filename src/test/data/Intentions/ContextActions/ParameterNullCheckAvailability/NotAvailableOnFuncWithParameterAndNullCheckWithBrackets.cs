class A
{
  public A(string arg1{off})
  {
	if (arg1 == null)
	{
		throw new ArgumentNullException("arg1");
	}
  }  
  
  public A(string arg1,string arg2{off})
  {
	if (arg1 == null)
	{
		throw new ArgumentNullException("arg1");
	}
	
	if (arg2 == null)
	{
		throw new ArgumentNullException("arg2");
	}
  } 
  
  public void A(string arg1{off})
  {
	if (arg1 == null)
	{
		throw new ArgumentNullException("arg1");
	}
  }  
  
  public void A(string arg1,string arg2{off})
  {
	if (arg1 == null)
	{
		throw new ArgumentNullException("arg1");
	}
	
	if (arg2 == null)
	{
		throw new ArgumentNullException("arg2");
	}
  } 
  
  
}