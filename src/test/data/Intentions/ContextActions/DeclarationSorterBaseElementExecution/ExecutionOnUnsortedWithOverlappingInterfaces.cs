namespace Test
{	
	interface IA
	{
	  void A();  
	  void B();
	  void C();
	}
	
	interface IB
	{
	  void B();
	  void D();
	  void E();
	}	
	
	class {caret} A : IB, IA 
	{
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}