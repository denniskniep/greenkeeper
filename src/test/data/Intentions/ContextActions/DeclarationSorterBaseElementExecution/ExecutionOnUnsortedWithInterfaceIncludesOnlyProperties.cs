class {caret} A : ITest
{  
  public string B {get; set;}
  public string C {get;}
  public string D {get; set;}
  public string E {get;}
}

interface ITest
{  
  string C {get;}
  string B {get;}
  string E {get;}  
  string D {get; set;}
}