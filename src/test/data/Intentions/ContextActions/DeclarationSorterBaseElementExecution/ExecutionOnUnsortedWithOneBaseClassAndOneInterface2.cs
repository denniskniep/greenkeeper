namespace Test
{	
	class IA
	{
	  public virtual void A()
	  {
	  }
	  
	  public virtual void B()
	  {
	  }
	  
	  public virtual void C()
	  {
	  }	 
	}

	interface IB
	{
	  void D();
	  void E();
	}	
	
	class {caret} A : IB, IA 
	{
	  public override void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public override void A()
	  {  
	  }
	  
	  public override void B()
	  {  
	  }  
	}
}