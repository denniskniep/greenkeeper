namespace First
{
	interface IA
	{
	  void A();  
	  void B();
	  void C();
	}

}

namespace Second
{	
	using First;
	
	interface IA
	{
	  void D();
	  void E();
	}	
	
	class {caret} A : First.IA, Second.IA
	{
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}