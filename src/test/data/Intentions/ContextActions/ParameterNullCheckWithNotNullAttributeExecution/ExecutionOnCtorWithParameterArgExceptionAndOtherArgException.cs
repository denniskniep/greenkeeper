class A
{
  public A(B arg1, B arg2{caret})
  {    
    if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg1.Test == null) throw new ArgumentNullException("arg1.Test");
  }
}

class B
{
	public string Test {get;}

}