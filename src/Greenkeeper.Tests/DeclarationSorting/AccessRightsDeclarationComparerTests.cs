﻿using System.Collections.Generic;
using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class AccessRightsDeclarationComparerTests
    {
        [TestCase(AccessRights.PUBLIC, AccessRights.PUBLIC, 0)]
        [TestCase(AccessRights.PUBLIC, AccessRights.INTERNAL, -1)]
        [TestCase(AccessRights.INTERNAL, AccessRights.PUBLIC, 1)]
        public void CompareTestAllAccessRightsInOrderIncluded(AccessRights x, AccessRights y, int result)
        {
            var order = new List<AccessRights> { AccessRights.PUBLIC, AccessRights.INTERNAL, AccessRights.PROTECTED, AccessRights.PRIVATE };
            Assert.AreEqual(result,Compare(x, y, order));
        }

        [TestCase(AccessRights.PUBLIC, AccessRights.PUBLIC, 0)]
        [TestCase(AccessRights.PUBLIC, AccessRights.INTERNAL, 0)]
        [TestCase(AccessRights.INTERNAL, AccessRights.PUBLIC, 0)]
        public void CompareTestNoneAccessRightsInOrderIncluded(AccessRights x, AccessRights y, int result)
        {
            var order = new List<AccessRights>();
            Assert.AreEqual(result,Compare(x, y, order));
        }

        [TestCase(AccessRights.PUBLIC, AccessRights.PUBLIC, 0)]
        [TestCase(AccessRights.PUBLIC, AccessRights.INTERNAL, -1)]
        [TestCase(AccessRights.INTERNAL, AccessRights.PUBLIC, 1)]
        [TestCase(AccessRights.INTERNAL, AccessRights.INTERNAL, 0)]
        [TestCase(AccessRights.INTERNAL, AccessRights.PRIVATE, 0)]
        [TestCase(AccessRights.PRIVATE, AccessRights.PROTECTED, 0)]
        public void CompareTestOnlyPublicAccessRightsInOrderIncluded(AccessRights x, AccessRights y, int result)
        {
            var order = new List<AccessRights> { AccessRights.PUBLIC };
            Assert.AreEqual(result, Compare(x, y, order));
        }

        private int Compare(AccessRights x, AccessRights y,IList<AccessRights> order)
        {
            var mockedX = MockDeclarationWithAccessRights(x);
            var mockedY = MockDeclarationWithAccessRights(y);
           
            var alphaComparer = new AccessRightsDeclarationComparer(order);
            return alphaComparer.Compare(mockedX, mockedY);
        }

        private DeclarationOrderElement MockDeclarationWithAccessRights(AccessRights accessRights)
        {
            var mocked = MockRepository.GenerateMock<IAccessRightsOwner,IDeclaration>();
            mocked.Stub(d => d.GetAccessRights()).Return(accessRights).Repeat.Once();
            return new DeclarationOrderElement((IDeclaration)mocked);
        }
    }
}
