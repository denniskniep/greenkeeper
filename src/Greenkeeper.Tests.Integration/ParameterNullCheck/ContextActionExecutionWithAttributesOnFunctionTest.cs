﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Greenkeeper.DeclarationSorting;
using Greenkeeper.ParameterNullCheck;
using Greenkeeper.Tests.Integration.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.ParameterNullCheck
{
    [TestFixture]
    public class ContextActionExecutionWithAttributesOnFunctionTest : CSharpContextActionAvailabilityTestBase<ParameterNullCheckContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "ParameterNullCheckWithAttributesOnFunctionAvailability"; }
        }
        
        protected override ParameterNullCheckContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            return ParameterNullCheckContextAction.Create(dataProvider, new ParameterNullCheckSettings { FunctionAttributesThatIndicatesNoNullCheck = "Test,TestMethod" });
        }

        [TestCase("NotAvailableOnFuncWithAttributeTest")]
        [TestCase("NotAvailableOnFuncWithAttributeTestMethod")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
