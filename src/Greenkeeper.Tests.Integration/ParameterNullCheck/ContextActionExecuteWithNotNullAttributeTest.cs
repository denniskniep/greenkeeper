﻿using Greenkeeper.ParameterNullCheck;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.ParameterNullCheck
{
    [TestFixture]
    public class ContextActionExecuteWithNotNullAttributeTest : CSharpContextActionExecuteTestBase<ParameterNullCheckContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "ParameterNullCheckWithNotNullAttributeExecution"; }
        }
        
        protected override ParameterNullCheckContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var settings = new ParameterNullCheckSettings
            {
                AddNotNullAttributeToParameter = true
            };
            return ParameterNullCheckContextAction.Create(dataProvider, settings);
        }

        [TestCase("ExecutionOnCtorWithOneParameter")]
        [TestCase("ExecutionOnCtorWithTwoParameters")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatements")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneArgExceptionAtWrongPosition")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneWrongArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneOtherArgExceptionExists")]
        [TestCase("ExecutionOnCtorWithTwoParametersAndStatementsOneWrongArgExceptionEqualExists")]
        [TestCase("ExecutionOnCtorWithValueTypeParameter")]
        [TestCase("ExecutionOnMethodWithOneParameter")]
        [TestCase("ExecutionOnCtorWithoutNullCheckInThisCtor")]
        [TestCase("ExecutionOnCtorWithTwoNullCheckInMultiThisCtor")]
        [TestCase("ExecutionOnCtorWithParameterArgExceptionAndOtherArgException")]
        [TestCase("ExecutionOnCtorWithNullOrEmptyCheckInMultiThisCtor")]
        [TestCase("ExecutionOnCtorWithReferenceEqualCheckInMultiThisCtor")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}