﻿using System;
using Greenkeeper.ParameterNullCheck;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.ParameterNullCheck
{
    [TestFixture]
    public class ContextActionAvailabilityTests : CSharpContextActionAvailabilityTestBase<ParameterNullCheckContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "ParameterNullCheckAvailability"; }
        }

        protected override ParameterNullCheckContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            return ParameterNullCheckContextAction.Create(dataProvider, new ParameterNullCheckSettings());
        }
        
        [TestCase("AvailableOnFuncWithParameter")]
        [TestCase("AvailableOnFuncWithParameterAndSomeNullCheck")]
        [TestCase("AvailableOnFuncWithParameterAndInvalidNullCheck")]
        [TestCase("NotAvailableOnFuncWithoutParameter")]
        [TestCase("NotAvailableOnFuncAccessRightNotPublic")]
        [TestCase("NotAvailableOnFuncWithParameterAndNullCheck")]
        [TestCase("NotAvailableOnFuncWithParameterAndNullCheckSwitched")]
        [TestCase("NotAvailableOnFuncWithParameterAndNullCheckWithBrackets")]
        [TestCase("NotAvailableOnFuncWithValueTypeParameter")]
        [TestCase("AvailableOnFuncWithParameterAndNotEqualNullCheck")]
        [TestCase("NotAvailableOnFuncWithOutputParameter")]
        [TestCase("NotAvailableOnCtorWithNullCheckInThisCtor")]
        [TestCase("NotAvailableOnCtorWithNullCheckInMultiThisCtor")]
        [TestCase("NotAvailableOnCtorWithNullCheckInBaseCtor")]
        [TestCase("NotAvailableOnCtorWithNullCheckInMultiBaseCtor")]
        [TestCase("NotAvailableOnCtorWithTwoNullCheckInMultiThisCtor")]
        [TestCase("NotAvailableOnCtorWithTwoNullCheckInMultiBaseCtor")]
        [TestCase("NotAvailableOnFuncWithParameterAndStringEmpty")]
        [TestCase("NotAvailableOnFuncWithParameterAndStringWhitespace")]
        [TestCase("NotAvailableOnFuncWithParameterAndReferenceEqual")]
        [TestCase("NotAvailableOnFuncWithParameterAndMixedChecks")]
        [TestCase("NotAvailableOnFuncWithCanBeNullAttribute")]
        [TestCase("AvailableOnCtorWithBaseCtorFromExternalAssembly")]
        [TestCase("NotAvailableOnFuncWithParameterAndOptionalNullParameter")]
        [TestCase("NotAvailableOnInterfaceMethod")]
        [TestCase("NotAvailableOnAbstractMethod")]
        public void TestExecution(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
