﻿using System;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.TextControl;

namespace Greenkeeper.Tests.Integration
{
    public static class CSharpContextActionDataProviderFactory
    {
        public static ICSharpContextActionDataProvider Create(ISolution solution, ITextControl textControl)
        {
            var dataProvider = (ICSharpContextActionDataProvider)new CSharpContextActionDataBuilder().Build(solution, textControl);
            if (dataProvider == null)
            {
                throw new NullReferenceException("dataProvider");
            }
            return dataProvider;
        }
    }
}
