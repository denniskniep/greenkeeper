﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionMultiExecuteAccessRightsUsageTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterMultiExecutionAccessRightsUsage"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var order = new List<AccessRights> { AccessRights.PUBLIC, AccessRights.INTERNAL, AccessRights.PROTECTED, AccessRights.PRIVATE };
            var accessRightsDeclaration = new AccessRightsDeclarationComparer(order);
            var usageDeclaration = new UsageOrderDeclarationComparer();
            var comparer = new MultiDeclarationComparer(
                new Collection<IDeclarationComparer>
                    {
                        accessRightsDeclaration,
                        usageDeclaration
                    });
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedMethods1")]
        [TestCase("ExecutionOnUnsortedMethods2")]
        [TestCase("ExecutionOnUnsortedMethods3")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
