﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionNoClassDeclarationConditionExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterNoClassDeclarationConditionExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var alphaNumDeclarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(new AlphanumericDeclarationComparer());
            var condiitionalAlphaNumDeclarationSorter = new ConditionalDeclarationSorter(new NoClassDeclarationCondition(), alphaNumDeclarationSorter);

            var multiDeclarationSorter = new MultiConditionalDeclarationSorter(
                new Collection<ConditionalDeclarationSorter>
                {
                    condiitionalAlphaNumDeclarationSorter
                });

            return DeclarationSorterContextAction.Create(dataProvider, multiDeclarationSorter);
        }

        [TestCase("ExecutionOnUnsorted1")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
