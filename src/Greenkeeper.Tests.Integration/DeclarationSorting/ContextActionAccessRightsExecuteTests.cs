﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionAccessRightsExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAccessRightsExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var order = new List<AccessRights> { AccessRights.PUBLIC, AccessRights.INTERNAL, AccessRights.PROTECTED, AccessRights.PRIVATE };

            var comparer = new AccessRightsDeclarationComparer(order);
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedMethods")]
        [TestCase("ExecutionOnUnsortedMembers")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
