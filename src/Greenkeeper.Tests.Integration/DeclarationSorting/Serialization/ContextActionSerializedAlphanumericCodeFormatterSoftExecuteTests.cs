﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedAlphanumericCodeFormatterSoftExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericWithCodeFormatExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithAlphanumericComparerAndCodeFormatterExecutionSoft"; }
        }

        [TestCase("ExecutionOnUnsortedNotFormattedForSoft")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }

}