﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedAttributeClassDeclarationConditionExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAttributeClassDeclarationConditionExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithAttributeClassDeclarationCondition"; }
        }

        [TestCase("ExecutionOnUnsortedNotMatchFirstCondition")]
        [TestCase("ExecutionOnUnsortedMatchFirstCondition")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
