﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using JetBrains.ReSharper.Psi.Search;

namespace Greenkeeper
{
    public class DeclarationUsageFindConsumer : IFindResultConsumer<DeclarationUsageFindResult>
    {
        private readonly ICollection<DeclarationUsageFindResult> _FindResults;

        public DeclarationUsageFindConsumer()
        {
            _FindResults = new Collection<DeclarationUsageFindResult>();
        }

        public DeclarationUsageFindResult Build(FindResult result)
        {
            var findResultReference = result as FindResultReference;
            if (findResultReference != null)
            {
                return new DeclarationUsageFindResult(findResultReference.DeclaredElement, findResultReference.Reference);
                
            }
            var findResultLateBoundReference = result as FindResultLateBoundReference;
            if (findResultLateBoundReference != null)
            {
                return new DeclarationUsageFindResult(findResultLateBoundReference.DeclaredElement,findResultLateBoundReference.Reference);
            }

            return null;
        }

        public FindExecution Merge(DeclarationUsageFindResult data)
        {
            _FindResults.Add(data);
            return FindExecution.Continue;
        }

        public ICollection<DeclarationUsageFindResult> GetFindResults()
        {
            return _FindResults.ToList();
        }
    }
}
