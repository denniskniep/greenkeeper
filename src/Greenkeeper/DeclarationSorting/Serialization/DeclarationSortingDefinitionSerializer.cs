﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class DeclarationSortingDefinitionSerializer
    {
        private string _Xml;
        private DeclarationSortingDefinitionProxy _DeclarationSortingDefinitionProxy;
     
        public DeclarationSortingDefinitionSerializer(string xml)
        {
            if (xml == null) throw new ArgumentNullException("xml");
            _Xml = xml;
        }

        public DeclarationSortingDefinitionProxy Deserialize()
        {
            RemoveComments();
            DeserializeProxy();
            return _DeclarationSortingDefinitionProxy;
        }

        private void RemoveComments()
        {
            var xDoc = XDocument.Parse(_Xml);
            var comments = xDoc.DescendantNodes().Where(n => n.NodeType == XmlNodeType.Comment).ToList();
            foreach (var xComment in comments)
            {
                xComment.Remove();
            }
            _Xml = xDoc.ToString();
        }

        private void DeserializeProxy()
        {
            var deserializer = new XmlSerializer(typeof(DeclarationSortingDefinitionProxy));
            using (var reader = new StringReader(_Xml))
            {
                _DeclarationSortingDefinitionProxy = (DeclarationSortingDefinitionProxy)deserializer.Deserialize(reader);
                reader.Close();
            }
        }
    }
}
