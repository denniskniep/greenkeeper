﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("DeclarationSortingDefinition")]
    public class DeclarationSortingDefinitionProxy
    {
        [XmlElement("DeclarationSorter")]
        public XmlSerializable<DeclarationSorterProxyBase> DeclarationSorterProxy { get; set; }

        [XmlElement("AdditionalExecutor")]
        public XmlSerializable<AdditionalExecutorProxyBase> AdditionalExecutorProxy { get; set; }

        public DeclarationSortingDefinition Create()
        {
            var declarationSorter = DeclarationSorterProxy.Serialized.CreateDeclarationSorter();

            IAdditionalExecutor additionalExecutor;
            if (AdditionalExecutorProxy == null)
            {
                additionalExecutor = new NoAdditionalExecutor();
            }
            else
            {
                additionalExecutor = AdditionalExecutorProxy.Serialized.CreateAdditionalExecutor();
            }
            return new DeclarationSortingDefinition(declarationSorter,additionalExecutor);
        }
    }
}
