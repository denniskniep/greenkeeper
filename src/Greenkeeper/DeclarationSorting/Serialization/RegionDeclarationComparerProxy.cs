﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("RegionDeclarationComparer")]
    public class RegionDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            return new RegionDeclarationComparer();
        }
    }
}
