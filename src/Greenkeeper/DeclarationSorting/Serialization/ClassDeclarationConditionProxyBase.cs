﻿namespace Greenkeeper.DeclarationSorting.Serialization
{
    public abstract class ClassDeclarationConditionProxyBase
    {
        public abstract IClassDeclarationCondition CreateClassDeclarationCondition();
    }
}
