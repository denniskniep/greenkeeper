﻿using System;
using System.Collections.Generic;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class TypeNameSplitter : IItemConverter<ElementTypeName>
    {
        public IList<IndexItem<ElementTypeName>> Split(string typeNames)
        {
            var splitter = new IndexItemSplitter<ElementTypeName>(typeNames, this);
            return splitter.ExecuteSplitting();
        }

        public ElementTypeName Convert(string value)
        {
            return (ElementTypeName)Enum.Parse(typeof(ElementTypeName), value, true);
        }
    }
}
