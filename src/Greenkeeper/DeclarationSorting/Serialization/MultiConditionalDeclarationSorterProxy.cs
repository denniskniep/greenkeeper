﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("MultiConditionalDeclarationSorter")]
    public class MultiConditionalDeclarationSorterProxy : DeclarationSorterProxyBase
    {
        [XmlElement("ConditionalDeclarationSorter")]
        public List<ConditionalDeclarationSorterProxy> ConditionalDeclarationSorter { get; set; }

        public override IDeclarationSorter CreateDeclarationSorter()
        {
            var declarationSorter = ConditionalDeclarationSorter.Select(d => d.CreateConditionalDeclarationSorter()).ToList();
            return new MultiConditionalDeclarationSorter(declarationSorter);
        }
    }
}
