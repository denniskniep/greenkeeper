﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("UsageOrderDeclarationComparer")]
    public class UsageOrderDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlElement("DeclarationComparer")]
        public XmlSerializable<DeclarationComparerProxyBase> DeclarationComparerProxy { get; set; }

        public override IDeclarationComparer CreateDeclarationComparer()
        {
            IDeclarationComparer comparer;
            if (DeclarationComparerProxy == null)
            {
                comparer = UsageOrderDeclarationComparer.GetDefaultDeclarationComparer();
            }
            else
            {
                comparer = DeclarationComparerProxy.Serialized.CreateDeclarationComparer();
            }
            return new UsageOrderDeclarationComparer(comparer);
        }
    }
}
