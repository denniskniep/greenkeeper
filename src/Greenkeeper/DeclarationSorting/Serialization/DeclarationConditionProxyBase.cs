﻿
namespace Greenkeeper.DeclarationSorting.Serialization
{
    public abstract class DeclarationConditionProxyBase
    {
        public abstract IDeclarationCondition CreateDeclarationCondition();
    }
}
