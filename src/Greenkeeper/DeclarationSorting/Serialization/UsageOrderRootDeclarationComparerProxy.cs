﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("UsageOrderRootDeclarationComparer")]
    public class UsageOrderRootDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            return new UsageOrderRootDeclarationComparer();
        }
    }
}
