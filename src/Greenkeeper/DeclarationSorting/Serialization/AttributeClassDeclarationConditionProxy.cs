﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("AttributeClassDeclarationCondition")]
    public class AttributeClassDeclarationConditionProxy : ClassDeclarationConditionProxyBase
    {
        [XmlAttribute("Attributes")]
        public string Attributes { get; set; }
        
        public override IClassDeclarationCondition CreateClassDeclarationCondition()
        {
            var splittedValidAttributes = SplitAttributes();
            return new AttributeClassDeclarationCondition(splittedValidAttributes);
        }

        private ICollection<string> SplitAttributes()
        {
            return new AttributeSplitter().Split(Attributes).Select(a => a.Item).ToList();
        }
    }
}
