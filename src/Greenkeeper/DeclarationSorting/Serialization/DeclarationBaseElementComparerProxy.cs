﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("DeclarationBaseElementComparer")]
    public class DeclarationBaseElementComparerProxy : DeclarationComparerProxyBase
    {
        [XmlAttribute("ExcludedTypes")]
        public string ExcludedTypes { get; set; }

        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var splittedTyped = new StringSplitter(ExcludedTypes ?? string.Empty).ExecuteSplitting();
            return new DeclarationBaseElementComparer(splittedTyped);
        }
    }
}
