﻿using System;
using System.Xml.Serialization;
using JetBrains.ReSharper.Psi.CodeStyle;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("CodeFormatExecutor")]
    public class CodeFormatExecutorProxy : AdditionalExecutorProxyBase
    {
        [XmlAttribute("Profile")]
        public string Profile { get; set; }

        public override IAdditionalExecutor CreateAdditionalExecutor()
        {
            CodeFormatProfile codeFormatProfile;
            if(Enum.TryParse(Profile, true, out codeFormatProfile))
            {
                return new CodeFormatExecutor(codeFormatProfile); 
            }
            return new CodeFormatExecutor(); 
        }
    }
}