﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public interface IDeclarationSorter
    {
        void InitWithClassDeclaration(IClassDeclaration classDeclaration);
        bool IsClassSorted();
        ICollection<SortedDeclaration> GetSortedDeclaration();
    }
}
