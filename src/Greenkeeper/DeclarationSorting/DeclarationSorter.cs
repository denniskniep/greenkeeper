﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSorter : IDeclarationSorter
    {
        private IClassDeclaration _ClassDeclaration;
        private readonly IDeclarationComparer _DeclarationComparer;
        private IList<IDeclaration> _Declarations;
        private IList<IDeclaration> _DeclarationsSorted;
        private ICollection<SortedDeclaration> _SortedDeclarations;
       
        public DeclarationSorter(IDeclarationComparer declarationComparer)
        {
            _DeclarationComparer = declarationComparer;
        }
        
        public void InitWithClassDeclaration(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");

            _ClassDeclaration = classDeclaration;
        }

        public bool IsClassSorted()
        {
            return !IsClassNotSorted();
        }
        
        private bool IsClassNotSorted()
        {
            return GetSortedDeclarationToMove().Any();
        }

        private ICollection<SortedDeclaration> GetSortedDeclarationToMove()
        {
            return GetSortedDeclaration().Where(s => s.NewIndex != s.OldIndex).ToList();
        }
        
        public ICollection<SortedDeclaration> GetSortedDeclaration()
        {
            ThrowExceptionIfClassDeclarationIsNull();
            Sort();
            return _SortedDeclarations;
        }

        private void ThrowExceptionIfClassDeclarationIsNull()
        {
            if (_ClassDeclaration == null)
            {
                throw new NullReferenceException("Init with ClassDeclaration first");
            }
        }
        
        private void Sort()
        {
            ExtractDeclarationsFromClass();
            SortDeclarationsWithDeclarationComparer();
            CreateSortDeclarations();
        }

        private void ExtractDeclarationsFromClass()
        {
            var extractor = new ClassDeclarationExtractor(_ClassDeclaration);
            _Declarations = extractor.GetDeclarations();
        }

        private void SortDeclarationsWithDeclarationComparer()
        {
            var declarationElements = GetDeclarationOrderElements();
            var sorter = new DeclarationOrderElementSorter(_DeclarationComparer, declarationElements);
            var sortedDeclarationElements = sorter.Sort();
            _DeclarationsSorted = sortedDeclarationElements.Select(d => d.Declaration).ToList();
        }

        private IList<DeclarationOrderElement> GetDeclarationOrderElements()
        {
            int i = 0;
            return _Declarations.Select(d => new DeclarationOrderElement(d,i++)).ToList();
        }

        private void CreateSortDeclarations()
        {
            _SortedDeclarations =  SortedDeclarationCreator.Create(_Declarations, _DeclarationsSorted);
        }
    }
}
