﻿using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class SortedDeclaration
    {
        public IDeclaration Declaration { get; private set; }
        public int OldIndex { get; private set; }
        public int NewIndex { get; private set; }

        public SortedDeclaration(IDeclaration declaration, int oldIndex, int newIndex)
        {
            Declaration = declaration;
            OldIndex = oldIndex;
            NewIndex = newIndex;
        }
    }
}
