﻿using JetBrains.Annotations;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationBaseElementComparer : DefaultDeclarationComparer
    {
        private readonly ICollection<string> _ExcludedTypes;
        private ICollection<DeclarationBaseElement> _DeclarationBaseElements;
        private IList<IDeclaredType> _SuperTypes;

        public DeclarationBaseElementComparer([NotNull] ICollection<string> excludedTypes)
        {
            _ExcludedTypes = excludedTypes;
            if (excludedTypes == null) throw new ArgumentNullException("excludedTypes");
        }

        public override void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarationOrderElements)
        {
            if (declarationOrderElements == null) throw new ArgumentNullException("declarationOrderElements");
            SetDeclarationBaseElements(declarationOrderElements);
            SetSuperTypes(declarationOrderElements);
            RemoveExcludedTypes();
        }

        private void SetDeclarationBaseElements(ICollection<DeclarationOrderElement> declarationOrderElements)
        {
            var declarations = declarationOrderElements.Select(d => d.Declaration).ToList();
            var finder = DeclarationBaseElementFinder.CreateFinder(declarations);
            _DeclarationBaseElements = finder.Find();
        }

        private void SetSuperTypes(ICollection<DeclarationOrderElement> declarationOrderElements)
        {
            if (declarationOrderElements.Count == 0)
            {
                _SuperTypes = new Collection<IDeclaredType>();
            }

            var firstDeclaration = declarationOrderElements.First();
            var classDeclaration = firstDeclaration.Declaration.PathToRoot().OfType<IClassDeclaration>().Single();

            var finder = new RecursiveSuperTypeFinder(classDeclaration);
            _SuperTypes = finder.Find().ToList();
        }

        private void RemoveExcludedTypes()
        {
            foreach (var superType in _SuperTypes.ToList())
            {
                if (_ExcludedTypes.Any(e => e.Equals(superType.GetClrName().FullName)))
                {
                    _SuperTypes.Remove(superType);
                }
            }
        }

        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");

            var superTypeIndexX = GetFirstSuperTypeIndex(x);
            var superTypeIndexY = GetFirstSuperTypeIndex(y);

            var compareResult = superTypeIndexX.CompareTo(superTypeIndexY);
            if (compareResult != 0)
            {
                return compareResult;
            }

            var positionX = GetPositionInSuperType(x, superTypeIndexX);
            var positionY = GetPositionInSuperType(y, superTypeIndexY);

            return positionX.CompareTo(positionY);
        }

        private int GetFirstSuperTypeIndex(DeclarationOrderElement element)
        {
            var declarationBaseElements = GetDeclarationBaseElements(element.Declaration.DeclaredElement);
            var indices = GetSuperTypeIndices(declarationBaseElements);

            if (indices.Count == 0)
            {
                return int.MaxValue;
            }
            return indices.Min();
        }

        private ICollection<DeclarationBaseElement> GetDeclarationBaseElements(IDeclaredElement declaredElement)
        {
            return _DeclarationBaseElements.Where(e => Equals(e.DeclaredElement, declaredElement)).ToList();
        }

        private ICollection<int> GetSuperTypeIndices(ICollection<DeclarationBaseElement> declarationBaseElements)
        {
            ICollection<int> indices = new Collection<int>();
            foreach (var declarationBaseElement in declarationBaseElements)
            {
                var superType = GetSuperType(declarationBaseElement);
                if (superType != null)
                {
                    var superTypeIndex = _SuperTypes.IndexOf(superType);
                    indices.Add(superTypeIndex);
                }
            }
            return indices;
        }
        
        private IDeclaredType GetSuperType(DeclarationBaseElement declarationBaseElement)
        {
            var superType = GetSuperType(declarationBaseElement.BaseDeclaredElement);
            return _SuperTypes.FirstOrDefault(o => Equals(o.GetClrName(), superType.GetClrName()));
        }

        private ITypeElement GetSuperType(IDeclaredElement baseDeclaredElement)
        {
            var typeMember = ConvertToTypeMember(baseDeclaredElement);
            return GetContainingType(typeMember);
        }

        private ITypeMember ConvertToTypeMember(IDeclaredElement baseDeclaredElement)
        {
            var typeMember = baseDeclaredElement as ITypeMember;
            if (typeMember == null)
            {
                throw new NullReferenceException("typeMember");
            }
            return typeMember;
        }

        private static ITypeElement GetContainingType(ITypeMember typeMember)
        {
            var containingType = typeMember.GetContainingType();
            if (containingType == null)
            {
                throw new NullReferenceException("containingType");
            }
            return containingType;
        }

        private int GetPositionInSuperType(DeclarationOrderElement element, int superTypeIndex)
        {
            var baseElements = GetDeclarationBaseElements(element.Declaration.DeclaredElement);
            if (baseElements.Count == 0 || superTypeIndex == int.MaxValue)
            {
                return int.MaxValue;
            }

            var foundBaseElement = GetSuperTypesDeclarationBaseElement(superTypeIndex, baseElements);
            if (foundBaseElement == null)
            {
                return int.MaxValue;
            }

            return GetPosition(foundBaseElement.BaseDeclaredElement);
        }

        private DeclarationBaseElement GetSuperTypesDeclarationBaseElement(int superTypeIndex, ICollection<DeclarationBaseElement> baseElements)
        {
            foreach (var baseElement in baseElements)
            {
                var superType = GetSuperType(baseElement);
                var superTypeAtIndex = _SuperTypes[superTypeIndex];
                if (Equals(superType, superTypeAtIndex))
                {
                    return baseElement;
                }
            }
            return null;
        }

        private int GetPosition(IDeclaredElement baseDeclaredElement)
        {
            var typeMember = ConvertToTypeMember(baseDeclaredElement);
            var containingType = GetContainingType(typeMember);

            return GetPosition(containingType, typeMember);
        }

        private static int GetPosition(ITypeElement containingType, ITypeMember typeMember)
        {
            int position = 0;
            foreach (var memberInContainigType in containingType.GetMembers())
            {
                if (Equals(memberInContainigType, typeMember))
                {
                    return position;
                }
                position++;
            }

            throw new Exception("typeMember does not exist in members of containigType");
        }
    }
}
