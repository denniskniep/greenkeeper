﻿using JetBrains.ReSharper.Psi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class UsageOrderDeclarationSorter
    {
        private readonly IList<DeclarationOrderElement> _DeclarationOrderElements;
        private readonly ICollection<DeclarationUsage> _DeclarationUsages;
        private IList<DeclarationOrderElement> _ElementsToProceed;
        private IList<DeclarationOrderElement> _SortedElements;
        private DeclarationOrderElement _CurrentFirstElement;

        public UsageOrderDeclarationSorter(ICollection<DeclarationOrderElement> declarationOrderElements, ICollection<DeclarationUsage> declarationUsages)
        {
            if (declarationOrderElements == null) throw new ArgumentNullException("declarationOrderElements");
            if (declarationUsages == null) throw new ArgumentNullException("declarationUsages");

            _DeclarationOrderElements = declarationOrderElements.ToList();
            _DeclarationUsages = declarationUsages;
        }

        public IList<DeclarationOrderElement> Sort()
        {
            _SortedElements = new List<DeclarationOrderElement>();
            _ElementsToProceed = _DeclarationOrderElements.ToList();

            while (HasNext())
            {
                AddElementAndCalledElementsFor(_CurrentFirstElement);
            } 
            
            return _SortedElements;
        }

        private bool HasNext()
        {
            _CurrentFirstElement = FindNext();
            return _CurrentFirstElement != null;
        }

        private DeclarationOrderElement FindNext()
        {
            return _ElementsToProceed.FirstOrDefault();
        }

        private void AddElementAndCalledElementsFor(DeclarationOrderElement currentElement)
        {
            AddToSortedElements(currentElement);
            var calls = GetCallsFromElement(currentElement);
            foreach (var declarationUsage in calls)
            {
                var calledElement = GetElementByDeclaredElement(declarationUsage.DeclaredElement);
                if (calledElement != null)
                {
                    AddElementAndCalledElementsFor(calledElement);
                }
            }
        }

        private void AddToSortedElements(DeclarationOrderElement declarationOrderElement)
        {
            _ElementsToProceed.Remove(declarationOrderElement);
            _SortedElements.Add(declarationOrderElement);
        }

        private IEnumerable<DeclarationUsage> GetCallsFromElement(DeclarationOrderElement declarationOrderElement)
        {
            return _DeclarationUsages
                .Where(d => Equals(d.UsedBy.DeclaredElement, declarationOrderElement.Declaration.DeclaredElement))
                .OrderBy(d => d.AtPosition.StartOffset.Offset);
        }
        
        private DeclarationOrderElement GetElementByDeclaredElement(IDeclaredElement declaredElement)
        {
            return _ElementsToProceed.FirstOrDefault(e => Equals(e.Declaration.DeclaredElement, declaredElement));
        }
    }
}
