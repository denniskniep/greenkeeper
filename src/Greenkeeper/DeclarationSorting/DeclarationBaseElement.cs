﻿using System;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationBaseElement
    {
        public IDeclaredElement DeclaredElement { get; private set; }
        public IDeclaredElement BaseDeclaredElement { get; private set; }
        
        public DeclarationBaseElement(IDeclaredElement declaredElement, IDeclaredElement baseDeclaredElement)
        {
            if (declaredElement == null) throw new ArgumentNullException("declaredElement");
            if (baseDeclaredElement == null) throw new ArgumentNullException("baseDeclaredElement");
            DeclaredElement = declaredElement;
            BaseDeclaredElement = baseDeclaredElement;
        }
    }
}
