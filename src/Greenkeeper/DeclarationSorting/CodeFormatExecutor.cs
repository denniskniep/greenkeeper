﻿using JetBrains.Application.Progress;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CodeStyle;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class CodeFormatExecutor : DefaultAdditionalExecutor
    {
        private readonly CodeFormatProfile _FormatProfile;

        public CodeFormatExecutor(CodeFormatProfile formatProfile = CodeFormatProfile.DEFAULT)
        {
            _FormatProfile = formatProfile;
        }

        public override void BeforeSort(IClassDeclaration classDeclaration)
        {
        }

        public override void AfterSort(IClassDeclaration classDeclaration)
        {
            var sourceFile = classDeclaration.GetSourceFile();
            if (sourceFile == null)
            {
                return;
            }
            var file = PsiFileFinder.FindCSharpPsiFile(sourceFile);
            var range = GetTreeTextRange(classDeclaration);
            file.FormatFileRange(range, _FormatProfile, NullProgressIndicator.Instance);
        }

        private static TreeTextRange GetTreeTextRange(IClassDeclaration classDeclaration)
        {
            var classStartOffset = classDeclaration.GetTreeStartOffset();
            var classLength = classDeclaration.GetTextLength();
            return new TreeTextRange(classStartOffset, classLength);
        }
    }
}
