﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class BrokenRegionRemover 
    {
        private readonly ICollection<RegionDeclarationRange> _RegionDeclarationRangesBeforeSort;
        private readonly ICollection<RegionDeclarationRange> _RegionDeclarationRangesAfterSort;
        private readonly TreeNodeWriter _TreeNodeWriter;

        public BrokenRegionRemover(ICollection<RegionDeclarationRange> regionDeclarationRangesBeforeSort, ICollection<RegionDeclarationRange> regionDeclarationRangesAfterSort,
                                   TreeNodeWriter treeNodeWriter)
        {
            if (regionDeclarationRangesBeforeSort == null)throw new ArgumentNullException("regionDeclarationRangesBeforeSort");
            if (regionDeclarationRangesAfterSort == null)throw new ArgumentNullException("regionDeclarationRangesAfterSort");
            if (treeNodeWriter == null) throw new ArgumentNullException("treeNodeWriter");
            _RegionDeclarationRangesBeforeSort = regionDeclarationRangesBeforeSort;
            _RegionDeclarationRangesAfterSort = regionDeclarationRangesAfterSort;
            _TreeNodeWriter = treeNodeWriter;
        }

        public void RemoveBrokenRegions()
        {
            var brokenRegions = DetectBrokenRegions();
            RemoveBrokenRegions(brokenRegions);
        }
        
        private ICollection<RegionDeclarationRange> DetectBrokenRegions()
        {
            var detector = new BrokenRegionDetector(_RegionDeclarationRangesBeforeSort, _RegionDeclarationRangesAfterSort);
            return detector.Detect();
        }

        private void RemoveBrokenRegions(ICollection<RegionDeclarationRange> brokenRegions)
        {
            var regions = brokenRegions.Select(r => r.Region).ToList();
            var remover = new RegionRemover(_TreeNodeWriter, regions);
            remover.Remove();
        }
    }
}
