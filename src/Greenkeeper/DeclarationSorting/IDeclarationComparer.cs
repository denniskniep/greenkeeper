﻿using System.Collections.Generic;

namespace Greenkeeper.DeclarationSorting
{
    public interface IDeclarationComparer : IComparer<DeclarationOrderElement>
    {
        void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarations);
        bool NeedResort();
        bool NeedSortedInitWithDeclarationOrderElements();
    }
}