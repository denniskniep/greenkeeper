﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationTypeNameExtractor
    {
        private readonly DeclarationOrderElement _DeclarationOrderElement;
        private readonly ICollection<IDeclarationTypeNameExtractorItem> _SpecialExtractorItems;

        public static DeclarationTypeNameExtractor Create(DeclarationOrderElement declarationOrderElement)
        {
            return new DeclarationTypeNameExtractor(declarationOrderElement,GetSpecialExtractorItems());
        }
        
        private static ICollection<IDeclarationTypeNameExtractorItem> GetSpecialExtractorItems()
        {
            return new Collection<IDeclarationTypeNameExtractorItem>
                {
                    new DestructorTypeNameExtractorItem(),
                    new IndexerTypeNameExtractorItem(),
                    new OperatorTypeNameExtractorItem()
                };
        }

        private DeclarationTypeNameExtractor(DeclarationOrderElement declarationOrderElement,
                                             ICollection<IDeclarationTypeNameExtractorItem> specialExtractorItems)
        {
            if (declarationOrderElement == null) throw new ArgumentNullException("declarationOrderElement");
            if (specialExtractorItems == null) throw new ArgumentNullException("specialExtractorItems");
            _DeclarationOrderElement = declarationOrderElement;
            _SpecialExtractorItems = specialExtractorItems;
        }

        public string GetTypeName()
        {
            var specialType = GetSpecialElementTypeName(_DeclarationOrderElement.Declaration);
            if (specialType != null)
            {
                return specialType;
            }

            var elementType = _DeclarationOrderElement.Declaration.DeclaredElement.GetElementType();
            return elementType.PresentableName;
        }

        private string GetSpecialElementTypeName(IDeclaration declaration)
        {
            foreach (var extractorItem in _SpecialExtractorItems)
            {
                if (extractorItem.IsType(declaration))
                {
                    return extractorItem.GetTypeName(declaration).ToLower();
                }
            }
            return null;
        }
    }
}
