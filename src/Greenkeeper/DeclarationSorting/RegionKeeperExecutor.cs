﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionKeeperExecutor : DefaultAdditionalExecutor
    {
        private ICollection<RegionDeclarationRange> _RegionDeclarationRangesBeforeSort;
        private IClassDeclaration _ClassDeclarationAfterSort;

        public override void BeforeSort(IClassDeclaration classDeclaration)
        {
            _RegionDeclarationRangesBeforeSort = DetectRegionDeclarationRange(classDeclaration);
        }

        private ICollection<RegionDeclarationRange> DetectRegionDeclarationRange(IClassDeclaration classDeclaration)
        {
            var detector = RegionDeclarationRangeDetector.CreateRegionDeclarationRangeDetector(classDeclaration);
            return detector.Detect();
        }

        public override void AfterSort(IClassDeclaration classDeclaration)
        {
            _ClassDeclarationAfterSort = classDeclaration;
            KeepRegions();
        }

        private void KeepRegions()
        {
            var keeper = new RegionKeeper(_ClassDeclarationAfterSort, _RegionDeclarationRangesBeforeSort, TreeNodeWriter);
            keeper.KeepRegions();
        }
    }
}
