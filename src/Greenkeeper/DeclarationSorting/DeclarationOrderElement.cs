﻿using System;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationOrderElement
    {
        public IDeclaration Declaration { get; private set; }
        public int? NativeIndex { get; private set; }
        public int? SortedIndex { get; private set; }

        public DeclarationOrderElement(IDeclaration declaration, int? nativeIndex = null, int? sortedIndex = null)
        {
            if (declaration == null) throw new ArgumentNullException("declaration");

            Declaration = declaration;
            NativeIndex = nativeIndex;
            SortedIndex = sortedIndex;
        }

        public void SetSortedIndex(int sortedIndex)
        {
            SortedIndex = sortedIndex;
        }
    }
}
