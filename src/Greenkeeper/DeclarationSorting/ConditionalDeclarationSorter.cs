﻿using System;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class ConditionalDeclarationSorter
    {
        private readonly IClassDeclarationCondition _Condition;
        private readonly IDeclarationSorter _DeclarationSorter;

        public ConditionalDeclarationSorter(IClassDeclarationCondition condition, IDeclarationSorter declarationSorter)
        {
            if (condition == null) throw new ArgumentNullException("condition");
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            _Condition = condition;
            _DeclarationSorter = declarationSorter;
        }

        public bool MatchCondition(IClassDeclaration classDeclaration)
        {
            return _Condition.CheckCondition(classDeclaration);
        }

        public IDeclarationSorter GetDeclarationSorter()
        {
            return _DeclarationSorter;
        }
    }
}
