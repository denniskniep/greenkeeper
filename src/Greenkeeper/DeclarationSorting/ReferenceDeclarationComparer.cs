﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public abstract class ReferenceDeclarationComparer<T> : DefaultDeclarationComparer where T : class
    {
        private readonly IList<IndexItem<T>> _ReferenceOrder;
        private int _MaxIndex;

        private ReferenceDeclarationComparer()
        {
            _ReferenceOrder = new List<IndexItem<T>>();
        }

       protected ReferenceDeclarationComparer(IList<IndexItem<T>> referenceOrder)
            : this()
        {
            if (referenceOrder == null) throw new ArgumentNullException("referenceOrder");
            SetIndexItems(referenceOrder);
            SetMaxIndex();
        }

        private void AddIndexItem(IndexItem<T> indexItem)
        {
            ThrowExceptionIfIndexItemExists(indexItem);
            _ReferenceOrder.Add(indexItem);
        }

        private void ThrowExceptionIfIndexItemExists(IndexItem<T> indexItem)
        {
            if (GetIndex(indexItem.Item) != -1)
            {
                throw new NotSupportedException("Item already exists");
            }
        }

        private void SetMaxIndex()
        {
            if (_ReferenceOrder.Count == 0)
            {
                _MaxIndex = 1;
                return;
            }
            _MaxIndex = _ReferenceOrder.Max(r => r.Index) + 1;
        }

        private void SetIndexItems(IList<IndexItem<T>> referenceOrder)
        {
            foreach (var item in referenceOrder)
            {
                AddIndexItem(item);
            }
        }

        protected static IList<string> GetEnumAsStrings<TE>(ICollection<TE> enumOrder)
        {
            if (enumOrder == null) throw new ArgumentNullException("enumOrder");
            return enumOrder.Select(a => a.ToString()).ToList();
        }

        protected static IList<IndexItem<string>> GetIndexEnumAsStrings<TE>(ICollection<IndexItem<TE>> enumOrder)
        {
            if (enumOrder == null) throw new ArgumentNullException("enumOrder");
            return enumOrder.Select(a => new IndexItem<string>(a.Item.ToString(), a.Index)).ToList();
        }

        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");

            var xIndex = GetIndex(x);
            var yIndex = GetIndex(y);
            var compResult = xIndex.CompareTo(yIndex);
            return compResult;
        }
        
        private int GetIndex(DeclarationOrderElement declarationOrderElement)
        {
            var references = GetReferencesFrom(declarationOrderElement);
            if (references.Count == 0)
            {
                return _MaxIndex;
            }
            
            return GetMinIndexFromReferenceOrder(references);
        }
        
        protected abstract ICollection<T> GetReferencesFrom(DeclarationOrderElement declarationOrderElement);
        
        private int GetMinIndexFromReferenceOrder(ICollection<T> references)
        {
            var minIndex = _MaxIndex;
            foreach (var reference in references)
            {
                var index = GetIndex(reference);
                if (index < minIndex && index != -1)
                {
                    minIndex = index;
                }
            }
            return minIndex;
        }

        private int GetIndex(T reference)
        {
            var indexItem = _ReferenceOrder.SingleOrDefault(r => Equals(r.Item, reference));
            if (indexItem == null)
            {
                return -1;
            }
            return indexItem.Index;
        }
    }
}
 