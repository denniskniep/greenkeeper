﻿using System;

namespace Greenkeeper.DeclarationSorting
{
    public class AlphanumericDeclarationComparer : DefaultDeclarationComparer
    {
        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");
            
            return String.CompareOrdinal(GetDeclaredName(x), GetDeclaredName(y));
        }

        private string GetDeclaredName(DeclarationOrderElement declarationOrderElement)
        {
            return declarationOrderElement.Declaration.DeclaredName;
        }
    }
}
