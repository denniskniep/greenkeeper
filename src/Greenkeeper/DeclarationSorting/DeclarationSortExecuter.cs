﻿using System;
using System.Collections.Generic;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSortExecuter
    {
        private readonly IClassDeclaration _ClassDeclaration;
        private readonly ISolution _Solution;
        private readonly IAdditionalExecutor _AdditionalExecutor;
        private readonly IDeclarationSorter _DeclarationSorter;
        private readonly ClassDeclarationReplacerProvider _TreeNodeProvider;
        private ICollection<SortedDeclaration> _SortedDeclarationsToMove;
        
        public DeclarationSortExecuter(IClassDeclaration classDeclaration, IDeclarationSorter declarationSorter, ISolution solution, IAdditionalExecutor additionalExecutor)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            if (solution == null) throw new ArgumentNullException("solution");
            if (additionalExecutor == null) throw new ArgumentNullException("additionalExecutor");

            _ClassDeclaration = classDeclaration;
            _DeclarationSorter = declarationSorter;
            _Solution = solution;
            _AdditionalExecutor = additionalExecutor;
            _TreeNodeProvider = new ClassDeclarationReplacerProvider(_ClassDeclaration,DeclarationReplacer.GetDefaultDeclarationReplaceItems());
        }

        public void ExecuteSortToClass()
        {
            using (var treeNodeWriter = new TreeNodeWriter(_ClassDeclaration, _TreeNodeProvider, _Solution))
            {
                ExecuteSortToClass(treeNodeWriter);
                treeNodeWriter.CommitTransaction();
            }
        }

        private void ExecuteSortToClass(TreeNodeWriter treeNodeWriter)
        {
            AdditionalExecuterBeforeSort(treeNodeWriter);
            SortDeclarations();
            MoveDeclarations(treeNodeWriter);
            AdditionalExecuterAfterSort();
        }

        private void AdditionalExecuterBeforeSort(TreeNodeWriter treeNodeWriter)
        {
            _AdditionalExecutor.InitWithTreeNodeWriter(treeNodeWriter);
            _AdditionalExecutor.BeforeSort(_ClassDeclaration);
        }

        private void SortDeclarations()
        {
            _DeclarationSorter.InitWithClassDeclaration(_ClassDeclaration);
            _SortedDeclarationsToMove = _DeclarationSorter.GetSortedDeclaration();
        }

        private void MoveDeclarations(TreeNodeWriter treeNodeWriter)
        {
            foreach (var sortedDeclaration in _SortedDeclarationsToMove)
            {
                var insertIndex = GetInsertationIndex(sortedDeclaration);
                var declaration = GetReplacedDeclaration(sortedDeclaration.Declaration);
                treeNodeWriter.InsertAtIndex(declaration, insertIndex);
                treeNodeWriter.RemoveTreeNode(declaration);
            }
        }

        private void AdditionalExecuterAfterSort()
        {
            _AdditionalExecutor.AfterSort(_ClassDeclaration);
        }

        private int GetInsertationIndex(SortedDeclaration sortedDeclaration)
        {
            var insertIndex = sortedDeclaration.NewIndex;
            if (IsDeclarationBeforeNewIndex(sortedDeclaration))
            {
                insertIndex++;
            }
            return insertIndex;
        }

        private bool IsDeclarationBeforeNewIndex(SortedDeclaration sortedDeclaration)
        {
            return GetCurrentDeclarationIndex(sortedDeclaration) < sortedDeclaration.NewIndex;
        }

        private int GetCurrentDeclarationIndex(SortedDeclaration sortedDeclaration)
        {
            var declarations = new ClassDeclarationExtractor(_ClassDeclaration).GetDeclarations();
            return declarations.IndexOf(sortedDeclaration.Declaration);
        }
        
        private ITreeNode GetReplacedDeclaration(IDeclaration declaration)
        {
            var replacer = DeclarationReplacer.CreateDefaultDeclarationReplacer();
            return replacer.GetReplacedDeclaration(declaration);
        }
    }
}
