﻿using System.Collections.ObjectModel;
using System.Linq;
using JetBrains.DocumentModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using System;

namespace Greenkeeper.DeclarationSorting
{
    public class ClassDeclarationRangeDetector
    {
        private readonly IClassDeclaration _ClassDeclaration;

        public ClassDeclarationRangeDetector(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            _ClassDeclaration = classDeclaration;
        }

        public DocumentRange GetHeaderRange()
        {
            var headerRange = _ClassDeclaration.NameIdentifier.GetDocumentRange();
            var possibleStartPositions = new Collection<int>
            {
                GetClassKeywordStartOffset(),
                GetFirstModifierStartOffset()
            };

            var headerStartPosition = possibleStartPositions.Min();
            return headerRange.SetStartTo(headerStartPosition);       
        }

        private int GetClassKeywordStartOffset()
        {
            return _ClassDeclaration.ClassKeyword.GetTreeStartOffset().Offset;
        }

        private int GetFirstModifierStartOffset()
        {
            if (_ClassDeclaration.ModifiersList == null || _ClassDeclaration.ModifiersList.FirstChild == null)
            {
                return GetClassKeywordStartOffset();
            }
            return _ClassDeclaration.ModifiersList.FirstChild.GetTreeStartOffset().Offset;
        }
    }
}
