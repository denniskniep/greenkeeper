﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DestructorTypeNameExtractorItem : IDeclarationTypeNameExtractorItem
    {
        public bool IsType(IDeclaration declaration)
        {
            return declaration is IDestructorDeclaration;
        }

        public string GetTypeName(IDeclaration declaration)
        {
            return ElementTypeName.Destructor.ToString();
        }
    }
}
