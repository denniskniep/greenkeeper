﻿using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    interface IDeclarationReplaceItem
    {
        bool MustReplace(IDeclaration declaration);
        ITreeNode GetReplacement(IDeclaration declaration);
    }
}
