﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class ConstantDeclarationReplaceItem : IDeclarationReplaceItem
    {
        public bool MustReplace(IDeclaration declaration)
        {
            return declaration is IConstantDeclaration && declaration.Parent is IMultipleConstantDeclaration;
        }

        public ITreeNode GetReplacement(IDeclaration declaration)
        {
            return declaration.Parent;
        }
    }
}
