﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionDeclarationComparer : DefaultDeclarationComparer
    {
        private ICollection<RegionDeclarationRange> _RegionDeclarationRanges;

        public override void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarations)
        {
            if (declarations.Count == 0)
            {
                _RegionDeclarationRanges = new Collection<RegionDeclarationRange>();
                return;
            }

            var declaration = declarations.First();
            var regionDetector = RegionDeclarationRangeDetector.CreateRegionDeclarationRangeDetector(declaration.Declaration);
            _RegionDeclarationRanges = regionDetector.Detect();
        }
        
        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            return GetRegionStartOffset(x).CompareTo(GetRegionStartOffset(y));
        }

        private int GetRegionStartOffset(DeclarationOrderElement declarationOrderElement)
        {
            var declaration = declarationOrderElement.Declaration;

            foreach (var regionDeclarationRange in _RegionDeclarationRanges)
            {
                if (IsRegionDeclarationRangeContainingDeclaration(regionDeclarationRange, declaration))
                {
                    return regionDeclarationRange.TreeTextRange.StartOffset.Offset;
                }
            }
            return 0;
        }

        private static bool IsRegionDeclarationRangeContainingDeclaration(RegionDeclarationRange regionDeclarationRange, IDeclaration declaration)
        {
            return regionDeclarationRange.ChildDeclarations.Any(d => Equals(d, declaration));
        }
    }
}
