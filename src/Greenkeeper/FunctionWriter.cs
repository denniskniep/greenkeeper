﻿using System;
using System.Linq;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper
{
    public class FunctionWriter : ElementWriter
    {
        private readonly ICSharpFunctionDeclaration _FunctionDeclaration;

        public FunctionWriter(ICSharpFunctionDeclaration functionDeclaration, ISolution solution)
            : base(functionDeclaration, solution)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            _FunctionDeclaration = functionDeclaration;
        }

        public void RemoveStatement(ICSharpStatement statement)
        {
            EnsureWriteWithLock(() => _FunctionDeclaration.Body.RemoveStatement(statement));
        }

        public void InsertAtIndex(string statement, int index)
        {
            InsertAtIndexTemplateMethod(statement, index);
        }

        protected override bool IsEmpty()
        {
            return _FunctionDeclaration.Body.Statements.Count == 0;
        }

        protected override bool IsEndOfElement(int index)
        {
            return index >= _FunctionDeclaration.Body.Statements.Count;
        }

        protected override void Insert(object element)
        {
            CreateBlockWithStatement((string)element);
        }

        private void CreateBlockWithStatement(string statement)
        {
            var block = ElementFactory.CreateBlock("{" + statement + "}");
            _FunctionDeclaration.SetBody(block);
        }

        protected override void InsertAfterLast(object element)
        {
            InsertAfterLastStatement((string) element);
        }

        private void InsertAfterLastStatement(string statement)
        {
            var lastStatement = _FunctionDeclaration.Body.Statements.Last();
            var cSharpStatement = CreateStatement(statement);
            _FunctionDeclaration.Body.AddStatementAfter(cSharpStatement, lastStatement);
        }

        private ICSharpStatement CreateStatement(string statement)
        {
            return ElementFactory.CreateStatement(statement);
        }

        protected override void InsertBefore(object element, int index)
        {
            InsertBeforeStatement((string) element, index);
        }
        
        private void InsertBeforeStatement(string statement, int index)
        {
            var statementAtIndex = GetStatementAtIndex(index);
            var cSharpStatement = CreateStatement(statement);
            _FunctionDeclaration.Body.AddStatementBefore(cSharpStatement, statementAtIndex);
        }

        private ICSharpStatement GetStatementAtIndex(int index)
        {
            return _FunctionDeclaration.Body.Statements[index];
        }

        
    }
}
