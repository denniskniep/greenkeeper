﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class InheritedConstructorContextItem : IExposerContextItem
    {
        public ICollection<IConstructorDeclaration> AlreadyProcessedConstructors { get; private set; }
        
        public InheritedConstructorContextItem()
        {
            AlreadyProcessedConstructors = new Collection<IConstructorDeclaration>();
        }
    }
}