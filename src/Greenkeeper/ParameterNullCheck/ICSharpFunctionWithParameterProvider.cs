﻿using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public interface ICSharpFunctionWithParameterProvider
    {
        ICSharpFunctionDeclaration GetFunctionDeclaration();
        ICSharpParametersOwnerDeclaration GetParametersOwnerDeclaration();
    }
}
