﻿using JetBrains.ReSharper.Feature.Services.CodeCleanup;
using System.ComponentModel;

namespace Greenkeeper.ParameterNullCheck
{
    [DefaultValue(false)]
    [DisplayName(ParameterNullCheckBulbAction.CheckAllParamsForNullName)]
    [Category(CSharpCategory)]
    public class ParameterNullCheckCleanUpDescriptor : CodeCleanupBoolOptionDescriptor
    {
        public ParameterNullCheckCleanUpDescriptor() : base(ParameterNullCheckHighlighting.SeverityId) { }
    }
}
