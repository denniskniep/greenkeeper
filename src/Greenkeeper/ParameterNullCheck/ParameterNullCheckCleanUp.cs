﻿using JetBrains.Application;
using JetBrains.Application.Progress;
using JetBrains.Application.Settings;
using JetBrains.DocumentModel;
using JetBrains.ReSharper.Feature.Services.CodeCleanup;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Files;
using NLog;
using System;
using System.Collections.Generic;

namespace Greenkeeper.ParameterNullCheck
{
    [CodeCleanupModule]
    public class ParameterNullCheckCleanUp : ICodeCleanupModule
    {
        private readonly Logger _Logger;
        private readonly ParameterNullCheckCleanUpDescriptor _Descriptor;
        private ParameterNullCheckSettings _Settings;
        private IContextBoundSettingsStore _SettingsStore;

        public ParameterNullCheckCleanUp(IShellLocks shellLocks)
        {
            _Descriptor = new ParameterNullCheckCleanUpDescriptor();
            _Logger = LogManager.GetCurrentClassLogger();
        }
        
        public ICollection<CodeCleanupOptionDescriptor> Descriptors
        {
            get { return new CodeCleanupOptionDescriptor[] { _Descriptor }; }
        }

        public bool IsAvailable(IPsiSourceFile sourceFile)
        {
            return PsiFileFinder.FindCSharpPsiFile(sourceFile) != null;
        }

        public bool IsAvailableOnSelection
        {
            get { return false; }
        }

        public PsiLanguageType LanguageType
        {
            get { return CSharpLanguage.Instance; }
        }

        private string GetCleanUpLogDescription()
        {
            return "ParameterNullCheckProcess";
        }

        public void Process(IPsiSourceFile sourceFile, IRangeMarker rangeMarker, CodeCleanupProfile profile,
                            IProgressIndicator progressIndicator)
        {
            ExceptionLogger.Execute(_Logger, GetCleanUpLogDescription, () => ProcessParameterNullCheckAndLogTrackedTime(sourceFile, rangeMarker, profile, progressIndicator));
        }

        private void ProcessParameterNullCheckAndLogTrackedTime(IPsiSourceFile sourceFile, IRangeMarker rangeMarker,
                                               CodeCleanupProfile profile, IProgressIndicator progressIndicator)
        {
            TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetCleanUpLogDescription, () => ProcessParameterNullCheck(sourceFile, rangeMarker, profile, progressIndicator));
        }

        private void ProcessParameterNullCheck(IPsiSourceFile sourceFile, IRangeMarker rangeMarker, CodeCleanupProfile profile, IProgressIndicator progressIndicator)
        {
            if (sourceFile == null) throw new ArgumentNullException("sourceFile");
            if (profile == null) throw new ArgumentNullException("profile");

            var file = PsiFileFinder.FindCSharpPsiFile(sourceFile);
            if (file == null)
                return;

            if (!profile.GetSetting(_Descriptor))
                return;

            LoadParameterNullCheckSettings(sourceFile);

            var functionNullCheckDetector = new ParameterNullCheckInFileRequiredDetector(file, _Settings);
            var functionsNeedNullCheck = functionNullCheckDetector.GetFunctionsNeedNullCheck();

            var solution = file.GetPsiServices().Solution;
            
            foreach (var function in functionsNeedNullCheck)
            {
                var inserter = new ParameterNullCheckInFunctionInserter(function, solution, _Settings);
                inserter.InsertParameterNullCheck();
            }
        }

        private void LoadParameterNullCheckSettings(IPsiSourceFile sourceFile)
        {
            _SettingsStore = sourceFile.GetSettingsStore();

            var loader = new ParameterNullCheckSettingsLoader(_SettingsStore);
            _Settings = loader.Load();
        }
        
        public void SetDefaultSetting(CodeCleanupProfile profile, CodeCleanup.DefaultProfileType profileType)
        {
            switch (profileType)
            {
                case CodeCleanup.DefaultProfileType.FULL:
                    profile.SetSetting(_Descriptor, true);
                    break;

                case CodeCleanup.DefaultProfileType.REFORMAT:
                    profile.SetSetting(_Descriptor, false);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("profileType");
            }
        }
    }
}
