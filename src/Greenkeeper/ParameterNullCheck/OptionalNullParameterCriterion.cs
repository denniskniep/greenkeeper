﻿using System;
using JetBrains.ReSharper.Psi.CSharp.Parsing;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class OptionalNullParameterCriterion : INullCheckRequiredCriterion
    {
        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");

            var regularParameter = parameterDeclaration as IRegularParameterDeclaration;
            if (regularParameter == null)
            {
                return true;
            }

            if (IsOptionalNullParameter(regularParameter))
            {
                return false;
            }
            return true;
        }

        private static bool IsOptionalNullParameter(IRegularParameterDeclaration regularParameter)
        {
            return regularParameter.IsOptional && IsNullLiteral(regularParameter.DefaultValue);
        }

        private static bool IsNullLiteral(IExpression expression)
        {
            var litExpression = expression as ICSharpLiteralExpression;
            return litExpression != null && litExpression.Literal != null &&
                   litExpression.Literal.GetTokenType() == CSharpTokenType.NULL_KEYWORD;
        }
    }
}
