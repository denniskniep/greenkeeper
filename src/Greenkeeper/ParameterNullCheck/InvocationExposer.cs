﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class InvocationExposer
    {
        private ICollection<string> _Argumnents;
        private ICSharpExpression _CSharpExpression;
        private string _InvocationText;

        public InvocationData Expose(ICSharpExpression cSharpExpression)
        {
            if (cSharpExpression == null) throw new ArgumentNullException("cSharpExpression");
            _CSharpExpression = cSharpExpression;
            SetExpressionParts();
            return new InvocationData(IsValid(), _InvocationText, _Argumnents);
        }

        private void SetExpressionParts()
        {
            var invocationExpression = _CSharpExpression as IInvocationExpression;

            if (invocationExpression != null)
            {
                _InvocationText = invocationExpression.InvokedExpression.GetText();
                _Argumnents = invocationExpression.Arguments.Select(a => a.GetText()).ToList();
            }
        }

        private bool IsValid()
        {
            return !String.IsNullOrEmpty(_InvocationText) && _Argumnents != null;
        }
    }
}
