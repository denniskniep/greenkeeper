﻿using JetBrains.ReSharper.Psi.CSharp.Parsing;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class StatementParameterEqualsNullExposer : StatementNullCheckExposer
    {
        private IReferenceExpression _CheckedParameter;
        private IEqualityExpression _EqualityExpression;
        private IfStatementThrowArgumentNullExceptionData _IfStatement;

        protected override bool IsNullCheckForParameter(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckForParameterConsiderOnlyCondition(cSharpStatement, parameterName)
                   && IsThrowedParameterName(parameterName);
        }

        protected override bool IsNullCheckForParameterConsiderOnlyCondition(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckStatement(cSharpStatement) &&
                   IsEqualityExpressionParameterName(parameterName);
        }

        private bool IsNullCheckStatement(ICSharpStatement cSharpStatement)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            SetStatementParts(cSharpStatement);
            return _IfStatement.IsValid && IsValidEqualityExpression();
        }

        private void SetStatementParts(ICSharpStatement cSharpStatement)
        {
            var exposer = new IfStatementThrowArgumentNullExceptionExposer();
            _IfStatement = exposer.Expose(cSharpStatement);
            _EqualityExpression = _IfStatement.Condition as IEqualityExpression;
            SetCheckedParameter();
        }

        private void SetCheckedParameter()
        {
            if (_EqualityExpression != null)
            {
                TrySetCheckedParameter(_EqualityExpression.LeftOperand);
                TrySetCheckedParameter(_EqualityExpression.RightOperand);
            }
        }

        private void TrySetCheckedParameter(IExpression expression)
        {
            var refExpression = expression as IReferenceExpression;
            if (refExpression != null)
            {
                _CheckedParameter = refExpression;
            }
        }

        private bool IsValidEqualityExpression()
        {
            return IsEqualEqualityExpression() && _CheckedParameter != null && ContainsEqualityExpressionNullLiteral();
        }

        private bool IsEqualEqualityExpression()
        {
            return _EqualityExpression != null && _EqualityExpression.EqualityType == EqualityExpressionType.EQEQ;
        }

        private bool ContainsEqualityExpressionNullLiteral()
        {
            return IsNullLiteral(_EqualityExpression.LeftOperand) || IsNullLiteral(_EqualityExpression.RightOperand);
        }

        private static bool IsNullLiteral(IExpression expression)
        {
            var litExpression = expression as ICSharpLiteralExpression;
            return litExpression != null && litExpression.Literal != null &&
                   litExpression.Literal.GetTokenType() == CSharpTokenType.NULL_KEYWORD;
        }

        private bool IsEqualityExpressionParameterName(string parameterName)
        {
            return _CheckedParameter.NameIdentifier != null &&
                   String.Equals(_CheckedParameter.NameIdentifier.Name, parameterName);
        }

        private bool IsThrowedParameterName(string parameterName)
        {
            return _IfStatement.ThrowedParameterName.Equals(parameterName);
        }
    }
}
