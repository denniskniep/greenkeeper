﻿using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    public static class StatementNullCheckExposerFactory
    {
        public static IStatementNullCheckExposer CreateExposer()
        {
            var exposers = new Collection<IStatementNullCheckExposer>
                {
                    new StatementParameterEqualsNullExposer(),
                    MethodInvocationWithOneArgumentExposer.CreateStringIsNullOrEmptyExposer(),
                    MethodInvocationWithOneArgumentExposer.CreateStringIsNullOrWhitespaceExposer(),
                    new StatementParameterReferenceEqualsNullExposer(),
                    new InheritedConstructorParameterNullCheckExposer()
                };
            return new StatementMultiNullCheckExposer(exposers);
        }
    }
}
