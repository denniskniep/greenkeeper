﻿using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NLog;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckBulbAction : IBulbAction
    {
        public const string CheckAllParamsForNullDescription = "Check all parameter for null in public ctor or method";
        public const string CheckAllParamsForNullName = "Check all parameter for null";
        private readonly ICSharpFunctionWithParameterProvider _FunctionWithParameter;
        private readonly Logger _Logger;
        private readonly ParameterNullCheckSettings _ParameterNullCheckSettings;

        public string Text
        {
            get { return CheckAllParamsForNullDescription; }
        }

        public ParameterNullCheckBulbAction(ICSharpFunctionWithParameterProvider functionWithParameter,
                                            ParameterNullCheckSettings parameterNullCheckSettings)
        {
            if (functionWithParameter == null) throw new ArgumentNullException("functionWithParameter");
            if (parameterNullCheckSettings == null) throw new ArgumentNullException("parameterNullCheckSettings");
            _FunctionWithParameter = functionWithParameter;
            _ParameterNullCheckSettings = parameterNullCheckSettings;
            _Logger = LogManager.GetCurrentClassLogger();
        }

        public void Execute(ISolution solution, ITextControl textControl)
        {
            if (solution == null) throw new ArgumentNullException("solution");
            if (textControl == null) throw new ArgumentNullException("textControl");

            ExceptionLogger.Execute(_Logger, GetBulbItemLogDescription, () => ExecuteInsertAndLogTrackedTime(solution));
        }

        private string GetBulbItemLogDescription()
        {
            return "ParameterNullCheckBulbItemExecute on " +
                   _FunctionWithParameter.GetFunctionDeclaration().DeclaredName;
        }

        private void ExecuteInsertAndLogTrackedTime(ISolution solution)
        {
            TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetBulbItemLogDescription, () => ExecuteInsert(solution));
        }

        private void ExecuteInsert(ISolution solution)
        {
            if (solution == null) throw new ArgumentNullException("solution");

            InsertForDeclaration(_FunctionWithParameter, solution);
        }

        private void InsertForDeclaration(ICSharpFunctionWithParameterProvider declaration, ISolution solution)
        {
            var inserter = new ParameterNullCheckInFunctionInserter(declaration, solution, _ParameterNullCheckSettings);
            inserter.InsertParameterNullCheck();
        }
    }
}
