﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class CSharpFunctionWithParameterProvider<T> : ICSharpFunctionWithParameterProvider where T : class, ICSharpFunctionDeclaration, ICSharpParametersOwnerDeclaration, ITreeNode
    {
        private readonly T _Declaration;

        public CSharpFunctionWithParameterProvider(T declaration)
        {
            _Declaration = declaration;
        }
        
        public ICSharpFunctionDeclaration GetFunctionDeclaration()
        {
            return _Declaration;
        }

        public ICSharpParametersOwnerDeclaration GetParametersOwnerDeclaration()
        {
            return _Declaration;
        }
    }
}
