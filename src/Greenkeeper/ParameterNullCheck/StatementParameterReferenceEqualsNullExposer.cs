﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Linq;

namespace Greenkeeper.ParameterNullCheck
{
    public class StatementParameterReferenceEqualsNullExposer : StatementNullCheckExposer
    {
        private const string _NullArgumentName = "null";
        private const string _ReferenceEqualsName = "ReferenceEquals";
        private string _CheckedParameter;
        private IfStatementThrowArgumentNullExceptionData _IfStatement;
        private InvocationData _Invocation;

        protected override bool IsNullCheckForParameter(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckForParameterConsiderOnlyCondition(cSharpStatement, parameterName)
                   && IsThrowedParameterName(parameterName);
        }

        protected override bool IsNullCheckForParameterConsiderOnlyCondition(ICSharpStatement cSharpStatement, string parameterName)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            if (parameterName == null) throw new ArgumentNullException("parameterName");

            return IsNullCheckStatement(cSharpStatement) &&
                   IsParameterName(parameterName);
        }

        private bool IsNullCheckStatement(ICSharpStatement cSharpStatement)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            SetStatementParts(cSharpStatement);
            return _IfStatement.IsValid && IsValidExpression();
        }

        private void SetStatementParts(ICSharpStatement cSharpStatement)
        {
            ExposeIfThrowStatement(cSharpStatement);
            ExposeInvocation();

            if (IsRefenreceEqualsCheck() && ContainsAnyNullArgument())
            {
                SetCheckedParameter();
            }
        }

        private void ExposeIfThrowStatement(ICSharpStatement cSharpStatement)
        {
            var ifThrowExposer = new IfStatementThrowArgumentNullExceptionExposer();
            _IfStatement = ifThrowExposer.Expose(cSharpStatement);
        }

        private void ExposeInvocation()
        {
            if (_IfStatement.Condition != null)
            {
                var invocationExposer = new InvocationExposer();
                _Invocation = invocationExposer.Expose(_IfStatement.Condition);   
            }
        }

        private bool IsRefenreceEqualsCheck()
        {
            return _Invocation != null && _Invocation.IsValid &&
                   _ReferenceEqualsName.Equals(_Invocation.InvocationText);
        }

        private bool ContainsAnyNullArgument()
        {
            return _Invocation.Arguments.Any(IsNullArgument);
        }

        private bool IsNullArgument(string argument)
        {
            return argument.Equals(_NullArgumentName);
        }

        private void SetCheckedParameter()
        {
            if (_Invocation.Arguments.Count == 2)
            {
                int paramIndex = 0;
                if (IsNullArgument(_Invocation.Arguments.First()))
                {
                    paramIndex = 1;
                }
                _CheckedParameter = _Invocation.Arguments.ToList()[paramIndex];
            }
        }

        private bool IsValidExpression()
        {
            return _Invocation != null && _CheckedParameter != null;
        }

        private bool IsParameterName(string parameterName)
        {
            return String.Equals(_CheckedParameter, parameterName);
        }

        private bool IsThrowedParameterName(string parameterName)
        {
            return _IfStatement.ThrowedParameterName.Equals(parameterName);
        }
    }
}