﻿using System.Collections.Generic;

namespace Greenkeeper.ParameterNullCheck
{
    public class ExposerContext : IExposerContext
    {
        private readonly Dictionary<string, IExposerContextItem> _ExposerContextItems;

        public ExposerContext()
        {
            _ExposerContextItems = new Dictionary<string, IExposerContextItem>();
        }
        
        public T GetItem<T>() where T : IExposerContextItem, new()
        {
            var typeName = GetIdentifier<T>();
            IExposerContextItem item;
            if (_ExposerContextItems.TryGetValue(typeName, out item))
            {
                return (T)item;
            }
            var newItem = Create<T>();
            return newItem;
        }

        private string GetIdentifier<T>() where T : IExposerContextItem, new()
        {
            return typeof(T).AssemblyQualifiedName;
        }

        private T Create<T>() where T : IExposerContextItem, new()
        {
            var item = new T();
            _ExposerContextItems.Add(GetIdentifier<T>(), item);
            return item;
        }
    }
}
