﻿using NLog;
using NLog.Targets;

namespace Greenkeeper
{
    [Target("VisualStudioOutput")] 
    public sealed class VisualStudioOutputTarget : TargetWithLayout
    {
        private readonly VisualStudioOutputWriter _VisualStudioOutputWriter;

        public VisualStudioOutputTarget()
        {
            _VisualStudioOutputWriter = new VisualStudioOutputWriter();
        }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = RenderLogMessage(logEvent);
            WriteLogMessage(logMessage);
        }

        private string RenderLogMessage(LogEventInfo logEvent)
        {
            return Layout.Render(logEvent);
        }

        private void WriteLogMessage(string logMessage)
        {
           _VisualStudioOutputWriter.WriteLine(logMessage);
        }
    } 
}
