﻿using JetBrains.DataFlow;
using JetBrains.UI.CrossFramework;
using JetBrains.UI.Options;
using System.Diagnostics;
using System.Windows.Navigation;

namespace Greenkeeper.Options
{
    [OptionsPage(PageId, "General", typeof(UnnamedThemedIcons.Greenkeeper), ParentId = RootOptionPage.PageId)]
    public partial class GeneralOptionPage : IOptionsPage
    {
        public const string PageId = "GreenkeeperGeneralOptions";

        public GeneralOptionPage(Lifetime lifetime, OptionsSettingsSmartContext settings)
        {
            InitializeComponent();
        }

        public EitherControl Control
        {
            get { return this; }
        }

        public string Id
        {
            get { return PageId; }
        }

        public bool OnOk()
        {
            return true;
        }

        public bool ValidatePage()
        {
            return true;
        }

        private void OnHyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
    
}
