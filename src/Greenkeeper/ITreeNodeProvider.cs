﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper
{
    public interface ITreeNodeProvider
    {
        IList<ITreeNode> GetTreeNodes();
    }
}
