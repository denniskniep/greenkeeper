﻿using EnvDTE;
using JetBrains.Application;
using JetBrains.Threading;
using System;

namespace Greenkeeper
{
    public class VisualStudioOutputWriter 
    {
        private const string _Greenkeeper = "Greenkeeper";
        private const string _Description = "Write message to output pane";
        private const string _WindowKindOutput = "{34E76E81-EE4A-11D0-AE2E-00A0C90FFFC3}";
        private DTE _Dte;

        public void WriteLine(string message)
        {
            Write(message+Environment.NewLine);
        }
        
        public void Write(string message)
        {
            ExecuteOnUiThread(() => CheckAndWriteToOutput(message));
        }

        private void CheckAndWriteToOutput(string message)
        {
            if (HasVisualStudio())
            {
                SetVisualStudio();
                WriteToOutput(message);
            }
        }
        
        private void WriteToOutput(string message)
        {
            var outputPane = ActivateOutputWindowPane();
            outputPane.OutputString(message);
        }

        private static bool HasVisualStudio()
        {
            return Shell.Instance.HasComponent<DTE>();
        }

        private void SetVisualStudio()
        {
            _Dte = Shell.Instance.GetComponent<DTE>();
        }  
        
        private OutputWindowPane ActivateOutputWindowPane()
        {
            var win = _Dte.Windows.Item(_WindowKindOutput);
            var outputWindow = (OutputWindow)win.Object;
            OutputWindowPane outputPane;
            try
            {
                outputPane = outputWindow.OutputWindowPanes.Item(_Greenkeeper);
            }
            catch
            {
                outputPane = outputWindow.OutputWindowPanes.Add(_Greenkeeper);
            }
            return outputPane;
        }

        private static void ExecuteOnUiThread(Action toExecute)
        {
            var threading = Shell.Instance.GetComponent<IThreading>();
            threading.ReentrancyGuard.ExecuteOrQueueEx(_Description, toExecute);
        }
    }
}
