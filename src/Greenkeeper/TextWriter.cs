﻿using JetBrains.Application;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using System;
using JetBrains.ReSharper.Psi.Transactions;

namespace Greenkeeper
{
    public class TextWriter : IDisposable
    {
        private readonly IPsiServices _PsiServices;
        private bool _TransactionCommitted;

        public TextWriter(ISolution solution)
        {
            if (solution == null) throw new ArgumentNullException("solution");

            _PsiServices = solution.GetPsiServices();
            StartTransaction();
        }

        private void StartTransaction()
        {
            _TransactionCommitted = false;
            _PsiServices.Transactions.StartTransaction("TextWriter_Transaction");
        }

        public void EnsureWriteWithLock(Action handler)
        {
            WriteWithLock(handler);
        }
      
        private void WriteWithLock(Action handler)
        {
            using (WriteLockCookie.Create(true))
            {
                handler();
            }
        }

        public TransactionResult CommitTransaction()
        {
            if (_TransactionCommitted)
            {
                throw new NotSupportedException("Transaction already committed");
            }
            _TransactionCommitted = true;
            return _PsiServices.Transactions.CommitTransaction();
        }

        public void Dispose()
        {
            if (_TransactionCommitted)
            {
                return;
            }
            _PsiServices.Transactions.RollbackTransaction();
        }
    }
}
